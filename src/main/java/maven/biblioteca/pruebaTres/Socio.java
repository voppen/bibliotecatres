package maven.biblioteca.pruebaTres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Socios")
public class Socio implements java.io.Serializable 
{
	
	private int socioDni;
	private String socioNombre;
	private String socioApellido;

	public Socio()
	{
		
	}

	public Socio(int dni, String nombre, String apellido) 
	{
		this.socioDni=dni;
		this.socioNombre = nombre;
		this.socioApellido = apellido;
	}

	@Id
	@Column(name = "SOCIO_DNI", unique = true, nullable = false, precision = 5, scale = 0)
	public int getSocioDni() 
	{
		return this.socioDni;
	}

	public void setSocioDni(int dni) 
	{
		this.socioDni=dni;
	}
	
	@Column(name = "SOCIO_NOMBRE", nullable = false, length = 50)
	public String getSocioNombre() 
	{
		return this.socioNombre;
	}

	public void setSocioNombre(String nombre) 
	{
		this.socioNombre = nombre;
	}

	@Column(name = "SOCIO_APELLIDO", nullable = false, length = 50)
	public String getSocioApellido() 
	{
		return this.socioApellido;
	}

	public void setSocioApellido(String apellido) 
	{
		this.socioApellido = apellido;
	}
	
	public String toString() { 
	    return "DNI: '" + this.socioDni + "' Nombre: '" + this.socioNombre + "' Apellido: '" + this.socioApellido + "'";
	} 
	
} 