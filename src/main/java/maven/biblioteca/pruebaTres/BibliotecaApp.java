package maven.biblioteca.pruebaTres;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;


import maven.biblioteca.persistence.HibernateUtil;


public class BibliotecaApp 
{
	
	 public static void main( String[] args )
	    {
	    	BasicConfigurator.configure();   
	    	 	
	        Session session = HibernateUtil.getSessionFactory().openSession();  	        
	    	
	        session.beginTransaction();
		
//			Libro libroUno = new Libro();
//			libroUno.setLibroIsbn(32989);
//			libroUno.setLibroAutor("Autor 4");
//			libroUno.setLibroTitulo("Titulo 4");
//			libroUno.setLibroTema("Tema 4");
//			
//			Socio socioUno = new Socio();
//			socioUno.setSocioDni(32984932);
//			socioUno.setSocioNombre("SocioNombreTres");
//			socioUno.setSocioApellido("SocioApellidoTres");
//			
//			Prestamo prestamoUno = new Prestamo();
//			prestamoUno.setPrestamoIsbn(32987);
//			prestamoUno.setPrestamoDni(32984932);
//			prestamoUno.setEstadoPrestamo('P');
//			
//	        session.save(libroUno);
//	        session.save(socioUno);
//	        session.save(prestamoUno);
//	        session.getTransaction().commit();
	        
	        //session.close();
	        
	        Libro libroRet = new Libro();
	        Socio socioRet = new Socio();
	        Prestamo prestamoRet = new Prestamo();
	        
	        session.beginTransaction();
	        libroRet = (Libro) session.get(Libro.class, 111);
	        socioRet = (Socio) session.get(Socio.class, 32984932);
	        prestamoRet = (Prestamo) session.get(Prestamo.class, 32987,32984932);
	        
	       
	        System.out.println("Libro" + libroRet.toString());
	    }
}
