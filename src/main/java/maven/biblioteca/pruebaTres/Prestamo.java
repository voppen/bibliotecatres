package maven.biblioteca.pruebaTres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Prestamos")

public class Prestamo implements java.io.Serializable
{
	private int prestamoIsbn;
	private int prestamoDni;
	private char estadoPrestamo;
	
	public Prestamo()
	{
		
	}
	
	public Prestamo(int isbn,int dni, char estado)
	{
		this.prestamoIsbn=isbn;
		this.prestamoDni=dni;
		this.estadoPrestamo=estado;
	}
	
	
	@Id
	@Column(name = "PRESTAMO_DNI", unique = true, nullable = false, precision = 5, scale = 0)
	public int getPrestamoDni() 
	{
		return this.prestamoDni;
	}
	
	public void setPrestamoDni(int dni) 
	{
		this.prestamoDni=dni;
	}
	
	@Id
	@Column(name = "PRESTAMO_ISBN", unique = true, nullable = false, precision = 5, scale = 0)
	public int getPrestamoIsbn() 
	{
		return this.prestamoIsbn;
	}
	
	public void setPrestamoIsbn(int dni) 
	{
		this.prestamoIsbn=dni;
	}

	
	@Column(name = "PRESTAMO_ESTADO", nullable = false, length = 50)
	public char getEstadoPrestamo() 
	{
		return this.estadoPrestamo;
	}

	public void setEstadoPrestamo(char estado) 
	{
		this.estadoPrestamo = estado;
	}
	
	public String toString() { 
	    return "Libro ISBN: '" + this.prestamoIsbn + "' Socio Dni: '" + this.prestamoDni + "' Estado Prestamo: '" + this.estadoPrestamo + "'";
	} 
	
	
}